$(function () {
    $('.custom-file-input').on('change', function() {
        let fileName = $(this).val().split('\\').pop()
        $(this).next('.custom-file-label').addClass('selected').html(fileName)
    });
});
const listOfStatus = [404, 405, 413, 419, 500];
// Set default validator
$.validator.setDefaults({
    errorElement: 'div',
    errorClass: 'is-invalid',
    validClass: 'is-valid',
    errorPlacement: function(error, element) {
        if ($(element).is('select')) {
            error.insertAfter($(element).next('.select2'));
        } else if ($(element).is('input[type="radio"]') || $(element).is('input[type="check"]')) {
            error.insertAfter($(element).next());
        } else if ($(element).is('input[type="checkbox"]') || $(element).is('input[type="check"]')) {
            error.insertAfter($(element).next());
        }
        else {
            error.appendTo($(element).closest('div'));
        }
    }
});
// Create swal mixins
const swalInfo = Swal.mixin({
    icon: 'success',
    toast: true,
    position: 'top-end',
    timer: 3000,
    showConfirmButton: false
});
const swalError = Swal.mixin({
    title: 'Error!!',
    icon: 'error',
    showConfirmButton: true
});
const swalAlert = Swal.mixin({
    cancelButtonColor: '#d33',
    confirmButtonColor: '#3085d6',
    confirmButtonText: 'Yes, continue!',
    showCancelButton: true,
    icon: 'warning',
    title: 'Sure?',
    text: 'Requests that were approved cannot be returned!.',
});
// Create helpers
function clearImage(element) {
    $(element).attr('src', '');
}
function clearValidate() {
    $('.form-control').attr('class', 'form-control');
    $('.custom-select').attr('class', 'custom-select');
    $('.custom-file-input').attr('class', 'custom-file-input');
    $('div.is-invalid, div.is-valid').remove();
}
function clearForm(form) {
    $(form)[0].reset();
    $('.custom-file-label').text('Choose File');
}
function loopErrors(errors) {
    $('.invalid-feedback').remove();
    for (error in errors) {
        $(`[name=${error}]`).removeClass('is-valid').addClass('is-invalid');
        $(`<div class="invalid-feedback">${errors[error][0]}</div>`).insertAfter($(`[name=${error}]`));
    }
}

// Books Management
const table = $('.table').DataTable({
    autoWidth : false,
    processing: true,
    serverSide: true,
    ajax: {
        url: '/books-management/data.json',
        type: 'post',
        data: { '_token': $('[name=csrf-token]').attr('content') }
    },
    columns: [
        { data: 'id', name: 'id', className: 'text-center' },
        { data: 'title', name: 'title' },
        { data: 'category', name: 'category' },
        { data: 'isbn', name: 'isbn', className: 'text-center' , sortable: false },
        { data: 'author', name: 'author' },
        { data: 'page_count', name: 'page_count', className: 'text-center' },
        { data: 'language', name: 'language' },
        { data: 'description', name: 'description', sortable: false },
        { data: 'path_cover', name: 'path_cover', sortable: false },
        { data: 'action', name: 'action', className: 'text-center', sortable: false },
    ],
    language: {
        loadingRecords: '<i class="fa fa-spinner fa-w-16 fa-spin fa-3x"></i> <span class="sr-only">Loading...</span>',
        processing: '<i class="fa fa-spinner fa-w-16 fa-spin fa-3x"></i> <span class="sr-only">Processing...</span>',
    }
});
(function getCategories() {
    $.getJSON('/books-management/categories')
        .done((response) => {
            let content = '';
            response.forEach(opt => {
                content += `
                    <option value="${opt.id}">${opt.category}</option>
                `;
            });
            $('[name=category_id]').append(content);
        })
        .fail((error) => {
            swalError.fire({ text: 'Book categories could not be loaded' });
        });
})();
function preview(element) {
    $('.path-cover').attr('src', window.URL.createObjectURL(element.files[0]));
}
function addForm() {
    $('.modal').modal('show');
    $('.modal-title').text('Add Form');
    $('.modal form').attr('action', '/books-management');
    $('[name=_method]').val('post');
    $('[name=path_cover]').attr('required', true);
    
    clearImage('.path-cover');
    clearValidate();
    clearForm('.modal form');
}
function editForm(id) {
    $('.modal').modal('show');
    $('.modal-title').text('Edit Form');
    $('.modal form')[0].reset();
    $('[name=_method]').val('put');
    $('[name=path_cover]').attr('required', false);
    
    clearImage('.path-cover');
    clearValidate();
    clearForm('.modal form');

    $.getJSON(`/books-management/${id}/edit`)
        .done((response) => {
            const { 
                title, 
                category_id, 
                isbn, 
                author, 
                page_count, 
                language, 
                description, 
                path_cover 
            } = response;

            $('.modal form').attr('action', `/books-management/${id}`);
            $('[name=title]').val(title);
            $('[name=category_id]').val(category_id);
            $('[name=isbn]').val(isbn);
            $('[name=author]').val(author);
            $('[name=page_count]').val(page_count);
            $('[name=language]').val(language);
            $('[name=description]').val(description);
            $('.path-cover').attr('src', `/storage/books/${path_cover}`);
        })
        .fail((error) => {
            swalError.fire({ text: 'Book detail details could not be loaded' });
        });
}
function submitForm(originalForm) {
    $(originalForm).validate({
        submitHandler: (form) => {
            $.post({
                url: $(form).attr('action'),
                data: new FormData(form),
                dataType: 'json',
                contentType: false,
                cache: false,
                processData: false,
            })
                .done((response) => {
                    swalInfo.fire(response.message);
                    table.ajax.reload();
                    $('.modal').modal('hide');
                })
                .fail((error) => {
                    if (listOfStatus.indexOf(error.status) > -1) swalError.fire({ text: error.statusText });
                    const errors = error.responseJSON.errors;
                    loopErrors(errors);
                });
        }
    });
}
function removeBook(id) {
    swalAlert.fire().then(({ isConfirmed }) => {
        if (isConfirmed) {
            $.post(`/books-management/${id}`, { 
                '_token': $('[name=csrf-token]').attr('content'),
                '_method': 'delete'
            })
                .done((response) => {
                    table.ajax.reload();
                    swalInfo.fire('Book selected successfully deleted');
                })
                .fail((error) => {
                    swalError.fire({ text: 'Cannot remove book selected' });
                });
        }
    });
}