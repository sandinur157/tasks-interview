<?php

use Illuminate\Database\Seeder;
use App\BookCategory;

class BookCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $bookCategories = [
            'Umum',
            'Filsafat dan Psikologi',
            'Agama',
            'Sosial',
            'Bahasa',
            'Sains dan Matematika',
            'Teknologi & Komputer',
            'Seni dan Rekreasi',
            'Literatur dan Sastra',
            'Sejarah dan Geografi'
        ];

        foreach ($bookCategories as $category) {
            BookCategory::create(compact('category'));
        }
    }
}
