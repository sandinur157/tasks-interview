<?php

use Illuminate\Support\Facades\Route;

Route::get('/', fn () => redirect('/login'));

/**
 * Main Routes
 */
Route::group(['middleware' => 'auth'], function () {
	Route::get('/books-management', 'BooksManagementController@index');
	Route::post('/books-management/data.json', 'BooksManagementController@listOfData');
	Route::post('/books-management', 'BooksManagementController@store');
	Route::get('/books-management/{book}/edit', 'BooksManagementController@edit');
	Route::put('/books-management/{book}', 'BooksManagementController@update');
	Route::get('/books-management/categories', 'BooksManagementController@categories');
	Route::delete('/books-management/{book}', 'BooksManagementController@destroy');
});

/**
 * Athentications
 */
Route::group(['namespace' => 'Auth'], function() {
    Route::get('/login', 'LoginController@showLoginForm')->name('login');
	Route::post('/login', 'LoginController@login');
	Route::post('/logout', 'LoginController@logout')->name('logout');
});