@extends('layouts.auth')
@section('title', 'Login')

@section('content')
<div class="login-box">
    <!-- /.login-logo -->
    <p class="login-box-msg mb-3">
        <img src="{{ asset('/images/logo.png') }}" alt="" class="img-fluid" width="200">
    </p>

    <div class="card shadow-none">
        <div class="card-body login-card-body">
            <form action="{{ route('login') }}" method="post">
                @csrf
                <div class="input-group has-feedback">
                    <input type="text" class="form-control form-control-sm @error('email') is-invalid @enderror" placeholder="Email" name="email" value="{{ old('email') }}" required autofocus>
                    <div class="input-group-append">
                        <div class="input-group-text">
                            <span class="fas fa-envelope"></span>
                        </div>
                    </div>
                </div>
                <span class="text-danger text-sm">
                    @error('email')
                        {{ $message }}
                    @enderror
                </span>
                <div class="input-group mt-3 has-feedback">
                    <input type="password" class="form-control form-control-sm" placeholder="Password" name="password" value="{!! old('password') !!}" required>
                    <div class="input-group-append">
                        <div class="input-group-text">
                            <span class="fas fa-lock"></span>
                        </div>
                    </div>
                </div>
                <span class="text-danger text-sm">
                    @error('password')
                        {{ $message }}
                    @enderror
                </span>
                <div class="row mt-3">
                    <div class="col-12">
                        <button class="btn btn-sm btn-info btn-block">Sign In</button>
                    </div>
                </div>
            </form>
        </div>
        <!-- /.login-card-body -->
    </div>
</div>
@endsection