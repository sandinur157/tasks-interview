@extends('layouts.master')
@section('title', 'Books Management')

@section('content')
<div class="row">
	<div class="col-lg-12">
		<div class="card card-info card-outline">
			<div class="card-header">
				<div class="card-title">List Of Books</div>
				<button type="button" class="btn btn-info btn-sticky" onclick="addForm()">
					<i class="fas fa-plus fa-w-16 fa-2x"></i>
				</button>
			</div>
			<div class="card-body table-responsive">
				<table class="table table-hover table-striped table-bordered">
					<thead>
						<th width="5%" class="text-center">#</th>
						<th>Title</th>
						<th>Category</th>
						<th>ISBN</th>
						<th>Author</th>
						<th>Page Count</th>
						<th>Language</th>
						<th>Description</th>
						<th>Path Cover</th>
						<th class="text-center"><i class="fas fa-cog"></i></th>
					</thead>
				</table>
			</div>
		</div>
	</div>
</div>
@endsection

@push('scripts')
<script src="/js/script.js"></script>
@includeIf('books_management.form')
@endpush
