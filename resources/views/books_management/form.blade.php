<div class="modal fade" id="modalForm" role="dialog" aria-labelledby="modalFormLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <form action="/books-management" method="post">
                <div class="modal-header">
                    <h5 class="modal-title" id="modalFormLabel"></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    @csrf @method('post')
                    <div class="form-group">
                        <label for="title">Title</label>
                        <input type="text" name="title" id="title" class="form-control" required> 
                    </div>
                    <div class="form-group row">
                        <div class="col-md-6">
                            <label for="category_id">Category</label>
                            <select name="category_id" id="category_id" class="custom-select">
                            </select>
                        </div>
                        <div class="col-md-6">
                            <label for="isbn">ISBN</label>
                            <input type="text" name="isbn" id="isbn" class="form-control" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="author">Author</label>
                        <input type="text" name="author" id="author" class="form-control" required> 
                    </div>
                    <div class="form-group row">
                        <div class="col-md-6">
                            <label for="page_count">Page Count</label>
                            <input type="number" name="page_count" id="page_count" class="form-control" required>
                        </div>
                        <div class="col-md-6">
                            <label for="language">Language</label>
                            <input type="text" name="language" id="language" class="form-control" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="description">Description</label>
                        <textarea name="description" id="description" class="form-control" required></textarea>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-6">
                            <label for="path_cover">Path Cover</label>
                            <div class="custom-file">
                                <input type="file" class="custom-file-input" id="path_cover" name="path_cover" onchange="preview(this)" required>
                                <label id="custom" class="custom-file-label" for="path_cover">Choose File</label>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <img class="path-cover mt-3 rounded shadow-sm" width="200">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary" onclick="submitForm(this.form)">Save</button>
                </div>
            </form>
        </div>
	</div>
</div>