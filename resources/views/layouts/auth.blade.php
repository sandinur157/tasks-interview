<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>{{ config('app.name') }} &mdash; @yield('title')</title>
    
    <link rel="icon" href="/images/logo.png">
    <link rel="stylesheet" href="/css/app.css">
</head>
<body class="hold-transition login-page container">
    
    @yield('content')

</body>
</html>