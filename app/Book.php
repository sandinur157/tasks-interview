<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    protected $guarded = [];

    public function bookCategory()
    {
        return $this->belongsTo(BookCategory::class, 'category_id', 'id');
    }
}
